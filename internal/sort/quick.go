package sort

func funcQuicksort(arr []int, start, end int) {
	if (end - start) < 1 {
		return
	}

	pivot := arr[end]   // выбор значения опорного элемента
	splitIndex := start // индекс разделяющего элемента

	// проходит по массиву от start до end
	// смещает splitIndex и размещает элементы так, что все элементы меньше pivot
	// оказываются слева от splitIndex и смещает значение sp

	// поиск всех элементов, которые меньше чем pivot
	// проходит splitIndex'ом по массиву и все элементы, которые меньше pivot
	// пока есть элементы, которые меньше чем pivot
	// перемещать в левую часть массива (типо сортировки выбором)
	// splitIndex уже занимает окончательную позицию, так как все что меньше слева от него
	// а он соответственно уже в большей части и все что справа от него >= ему
	for i := start; i < end; i++ {
		if arr[i] < pivot {
			temp := arr[splitIndex]
			arr[splitIndex] = arr[i]
			arr[i] = temp

			splitIndex++
		}
	}

	arr[end] = arr[splitIndex]
	arr[splitIndex] = pivot

	funcQuicksort(arr, start, splitIndex-1)
	funcQuicksort(arr, splitIndex+1, end)
}

// Быстрая сортировка
func QuickSort(input []int) []int {
	arr := make([]int, len(input))
	copy(arr, input)

	funcQuicksort(arr, 0, len(arr)-1)
	return arr
}
