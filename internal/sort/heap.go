package sort

// Создание пирамиды
func downHeap(input []int, k, n int) []int {
	arr := make([]int, len(input))
	copy(arr, input)

	var child int
	new_elem := arr[k]
	for k <= n/2 {
		child = 2 * k
		if child < n && arr[child] < arr[child+1] {
			child++
		}

		if new_elem >= arr[child] {
			break
		}

		arr[k] = arr[child]
		k = child
	}
	arr[k] = new_elem
	return arr
}

// Пирамидальная сортировка по убыванию
func HeapSort(input []int) []int {
	arr := make([]int, len(input))
	copy(arr, input)
	size := len(arr)

	for i := size / 2; i >= 0; i-- {
		arr = downHeap(arr, i, size-1)
	}

	var tmp int
	for i := size - 1; i > 0; i-- {
		tmp = arr[i]
		arr[i] = arr[0]
		arr[0] = tmp

		arr = downHeap(arr, 0, i-1)
	}

	return arr
}
