package main

import (
	"bitbucket.org/ducktyst/msoft_go_cource_2020/internal/sort"
	"fmt"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Необходимо указать сортировку. Варианты: bubble, heap, quick")
		os.Exit(1)
	}
	sortType := os.Args[1]

	inputArray := make([]int, len(os.Args[2:]))
	var err error
	for i, v := range inputArray {
		inputArray[i], err = strconv.Atoi(v)
		if err != nil {
			fmt.Println("Invalid array element")
			os.Exit(1)
		}
	}

	var sortedArray []int
	switch sortType {
	case "bubble":
		sortedArray = sort.BubbleSort(inputArray)
	case "heap":
		sortedArray = sort.HeapSort(inputArray)
	case "quick":
		sortedArray = sort.QuickSort(inputArray)
	default:
		fmt.Println("Неизвестный тип сортировки. Варианты: bubble, heap, quick")
		return
	}
	fmt.Println("Вывод:", sortedArray)
}
